//
//  Person.swift
//  Quiztastic
//
//  Created by Adam Leo on 2019-11-18.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import Foundation

struct Person{
    let name: String
    let location: String
    let score: String
    let category: String
}
