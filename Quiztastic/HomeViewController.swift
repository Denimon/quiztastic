//
//  ViewController.swift
//  Quiztastic
//
//  Created by Omar Zidan on 10/11/19.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import UIKit


class HomeViewController: UIViewController {
    //MARK: Outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var recentScoresTableView: UITableView!
    @IBOutlet weak var newGameButton: UIButton!
    
    //MARK: Properties
    var twoHighestPlayers : [Person] = []

    //MARK: View setup
    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.layer.insertSublayer(Aesthetics().getGradiantBackground(view: self.mainView), at: 0)
        NavigationBarProperties().setNavBarTranslucent(viewController: self)
        newGameButton.alpha = 0.1
        newGameButton.isEnabled = false
        
        recentScoresTableView.dataSource = self
        recentScoresTableView.delegate = self
        
        //MARK: Fetching questions from API
        
        if QuestionsStorage.globalQuestionStorage.databaseIsEmpty(){  //only runs if the local database is empty
            var apiReturns: Int = 0
            for index in 1..<Category.categories.count{
                
                QuizRequest.globaLQuizRequest.setup(amountOfQuestions: 50,category: Category.getCategoryIdByname(categoryName: Category.categories[index]))
                QuizRequest.globaLQuizRequest.getQuiz { result in
                    switch result {
                    case .failure(let error):
                        print(error)
                        DispatchQueue.main.async {self.errorHandlingApi(error: error)}
                        
                    case .success(let questions):
                        if questions.count == 0 {
                            DispatchQueue.main.async {self.errorHandlingApi(error: nil)}
                        }
                        else{
                            QuestionsStorage.globalQuestionStorage.addCategoryToCoreData(nameOfCategory: Category.categories[index], quizQuestions: questions)
                            apiReturns += 1
                            
                            if apiReturns == Category.categories.count - 1{
                                DispatchQueue.main.async{
                                    UIView.animate(withDuration: 3, animations: {
                                        self.newGameButton.alpha = 1
                                    }) { _ in
                                        self.newGameButton.isEnabled = true
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else{
            self.newGameButton.alpha = 1
            self.newGameButton.isEnabled = true
        }
 
    }
    //MARK: Error handling API
    func errorHandlingApi(error: QuizRequest.QuizError?){
        var errorMsg: String
        if QuizRequest.QuizError.noDataAvailable == error {
            errorMsg = "Check your internet connection"
        }
        else if QuizRequest.QuizError.canNotProcessData == error {
            errorMsg = "Failed to fetch data"
        }else{
            errorMsg = "Internal system error"
        }
        let alert = UIAlertController(title: "Network error", message: errorMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title:"Back", style: .default, handler:nil))
        self.present(alert, animated: true, completion: nil)
    }

    //MARK: Unwind action
    @IBAction func unwindToRootViewController(segue: UIStoryboardSegue) {
        
    }
}



//MARK: UITableViewDelegate

extension HomeViewController : UITableViewDelegate, UITableViewDataSource {
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.twoHighestPlayers = HighscoreStorage().getTwoHighestScore()
        self.recentScoresTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return twoHighestPlayers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellString = "cellForRecentScore"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellString, for: indexPath) as? HighscoreTableViewCell {
            cell.score.text = self.twoHighestPlayers[indexPath.row].score
            cell.name.text = self.twoHighestPlayers[indexPath.row].name
            cell.place.text = self.twoHighestPlayers[indexPath.row].location
            cell.categoryIcon.image = UIImage(named: Category.categoryImg[self.twoHighestPlayers[indexPath.row].category] ?? "Logo")
            
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: cell.frame.width, height: cell.frame.height))
            let image = UIImage(named: "normalCell")
            imageView.image = image
            cell.backgroundView = imageView
            return cell
            
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}
