//
//  Category.swift
//  Quiztastic
//
//  Created by Adam Leo on 2019-12-02.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import Foundation

struct Category{
    
    enum category: Int{
        case All = 0, Music, Geography, Sports, History, Television, Science
    }
    
    static let categoryImg = ["None": "Logo","Music": "disc", "Geography": "earth-globe", "Sports": "baseball-circular-ball", "History": "castle-with-a-flag-on-top", "Television": "television", "Science": "radiation-circular-symbol-with-three-rays"]
    
    static let categories = ["All", "Music", "Geography", "Sports", "History", "Television", "Science"]
    
    // Get the id that corresponds to a specific category in the API
    static func getCategoryIdByname(categoryName: String) -> Int{
        switch categoryName {
        case "Music":
            return 12
        case "Geography":
            return 22
        case "Sports":
            return 21
        case "History":
            return 23
        case "Television":
            return 14
        case "Science":
            return 17
        default:
            return 0
        }
    }
}
