//
//  QuizModel.swift
//  Quiztastic
//
//  Created by Johan Kantola on 2019-11-19.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import Foundation

struct listOfQuizQuestions: Decodable {
    var results: [quizQuestion]
}

struct quizQuestion: Decodable {
    var question: String
    var correct_answer: String
    var incorrect_answers: [String]
    init(question: String = "", correct_answer: String = "", incorrect_answers: [String] = []) {
        self.question = question
        self.correct_answer = correct_answer
        self.incorrect_answers = incorrect_answers
    }
}
