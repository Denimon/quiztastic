//
//  GameViewController.swift
//  Quiztastic
//
//  Created by Omar Zidan on 2019-11-13.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {
    //MARK: Outlets
    @IBOutlet var quizView: UIView!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var firstBtn: UIButton!
    @IBOutlet weak var secondBtn: UIButton!
    @IBOutlet weak var thirdBtn: UIButton!
    @IBOutlet weak var fourthBtn: UIButton!
    
    //MARK: Properties
    var allButtons: [UIButton] = []
    let nrOfButtons = 4
    let totalQuestions = 10
    var gameState: GameState!
    var pickedQuestionFromGameState: quizQuestion = quizQuestion()
    var timeLeft = 100.0
    var userDidAnswer = false
    var userAnsweredCorrect = false
    
    //MARK: Button press event
    @IBAction func answerBtnPressed(_ sender: UIButton) {
        // Handle press event of all answer buttons and checks if the correct answer was chosen
        userDidAnswer = true
        userAnsweredCorrect = sender.title(for: .normal) == pickedQuestionFromGameState.correct_answer ? true: false
    }

    //MARK: View setup
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set backgound, hide back button and set navbar title to which question the user is at
        quizView.layer.insertSublayer(Aesthetics().getGradiantBackground(view: quizView), at: 0)
        NavigationBarProperties().setNavBarTranslucent(viewController: self)
        navigationItem.hidesBackButton = true
        title = "\(gameState.questionsAnswered + 1)/\(totalQuestions)"
        // Display the timer's start value as seconds (instead of tenths of a second)
        timerLabel.text = String(timeLeft / 10.0)
        allButtons = [firstBtn, secondBtn, thirdBtn, fourthBtn]
        // Let the answer on each button be displayed on mutliple rows
        for button in allButtons {
            button.titleLabel?.numberOfLines = 0
        }
        setupQuestion()
        startQuestionTimer()
    }
    //MARK: Game setup
    func setupQuestion(){
        // Of the remaining questions, pick a random one as current
        let randomQuestionIndex = Int.random(in: 0..<gameState.totalQuestions - gameState.questionsAnswered)
        pickedQuestionFromGameState = gameState.quizQuestions[randomQuestionIndex]
        gameState.quizQuestions.remove(at: randomQuestionIndex)
        questionLabel.text = pickedQuestionFromGameState.question
        // Assign each available answer to a random (unique) button
        for _ in 0..<nrOfButtons {
            let randomButtonIndex = Int.random(in: 0..<allButtons.count)
            if(pickedQuestionFromGameState.incorrect_answers.count > 0){
                allButtons[randomButtonIndex].setTitle(pickedQuestionFromGameState.incorrect_answers.popLast(), for: .normal)
            }
            else{
                allButtons[randomButtonIndex].setTitle(pickedQuestionFromGameState.correct_answer, for: .normal)
            }
            allButtons.remove(at: randomButtonIndex)
        }

    }
    //MARK: Game logic
    func startQuestionTimer() {
        // Game logic; game state is checked and updated every .1 sec
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { questionTimer in
            // Display time left as seconds (instead of tenths of a second)
            self.timerLabel.text = String(self.timeLeft / 10.0)
            
            // Animate timer to alert user when time is almost up
            if self.timeLeft == 30.0 {
                self.timerLabel.textColor = UIColor.red
                self.scaleTimerLabel(targetLabel: self.timerLabel)
            }
            
            // If the user pressed an answer button or the time is up the current question is done
            if (self.userDidAnswer == true  || self.timeLeft == 0) {
                questionTimer.invalidate()
                self.gameState.questionsAnswered += 1
                if self.userAnsweredCorrect {
                    self.gameState.rightAnswers += 1
                    self.gameState.totalScore += 100 + Int(self.timeLeft * 0.5)
                }
                // If all questions are answered, move on to post game screen, else get next question
                if self.gameState.questionsAnswered == self.gameState.totalQuestions {
                    self.performSegue(withIdentifier: "segueToPostGame", sender: self)
                }
                else {
                    self.performSegue(withIdentifier: "gameSegueToSelf", sender: self)
                }
            }
            self.timeLeft -= 1.0
        }
    }
    
    // Timer alert animation
    func scaleTimerLabel(targetLabel: UILabel, duration: Double = 0.1) {
        let scale: CGFloat = 1.4
        UILabel.animate(withDuration: duration, delay: 0, options: [.curveEaseInOut, .repeat, .autoreverse], animations: {
            targetLabel.transform = CGAffineTransform(scaleX: scale, y: scale)
        }, completion: nil)
    }
    
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gameSegueToSelf" {
            if let gameViewController = segue.destination as? GameViewController {
                gameViewController.gameState = self.gameState
            }
        } else if segue.identifier == "segueToPostGame" {
            if let postGameViewController = segue.destination as? PostGameViewController {
                postGameViewController.gameState = self.gameState
            }
        }
    }
}

