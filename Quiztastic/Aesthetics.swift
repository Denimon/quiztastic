//
//  Aesthetics.swift
//  Quiztastic
//
//  Created by Adam Leo on 2019-11-11.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import UIKit

class Aesthetics{
        
    func getGradiantBackground(view: UIView) -> CAGradientLayer{
        let color1 = UIColor(red:0.41, green:0.48, blue:0.55, alpha:1)
        let color2 = UIColor(red:0.31, green:0.37, blue:0.45, alpha:1)
        let gradiant = CAGradientLayer()
        gradiant.frame = view.bounds
        gradiant.colors = [color1.cgColor, color2.cgColor]
        gradiant.startPoint = CGPoint(x: 0,y: 0)
        gradiant.endPoint = CGPoint(x: 1,y: 1)
        return gradiant 
    }
        
}
