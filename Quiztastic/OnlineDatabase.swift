//
//  OnlineServer.swift
//  Quiztastic
//
//  Created by Adam Leo on 2019-11-14.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import Foundation

class OnlineDatabase{
    
    let password = "quiztastic"
    func uploadScore(name: String, location: String, score: String, category: String, couldUpload: @escaping (_ sucess: Bool)->Void){
        let queryString = "?password=\(password)&name=\(name)&location=\(location)&score=\(score)&category=\(category)"
        //returns a new string that replaces the characters that is outside the ascii 7 bits range
        let queryStringEncoded = queryString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        let url = URL(string: "http://adamssite.net/addScore\(queryStringEncoded ?? "")")
        var request = URLRequest(url: url!)
        request.httpMethod = "POST"
        
        let uploadJob = URLSession.shared.dataTask(with: request){
            data, _, error in
            
            if error != nil {
                //Could not communicate with server at all
                print("ERR: \(error!)")
                couldUpload(false)
            }else{
                if let unwrappedData = data {
                    let returnedData = NSString(data: unwrappedData, encoding: String.Encoding.utf8.rawValue)
                    if returnedData == "1"
                    {
                        //Could communicate with server and everything went as expected
                        couldUpload(true)
                    }else{
                        print("error on the web application when tyring to upload data")
                        couldUpload(false)
                    }
                }
            }
        }
        uploadJob.resume()
    } 
    
    func getScores(category: String?, returnScores: @escaping (_ people: [Person]?)->Void){
        
        let url = category == "All" ? URL(string: "http://adamssite.net/getScores")
            : URL(string: "http://adamssite.net/getScoresWithCategory/\(category!)")
        
        let request = URLRequest(url: url!)
        
        let getJob = URLSession.shared.dataTask(with: request){
            data, _, error in
            
            if error != nil {
                print("ERR: \(error!)")
                returnScores(nil)
            }else{
                if let unwrappedData = data {
                    if NSString(data: unwrappedData, encoding: String.Encoding.utf8.rawValue) != "0" {
                        //Ideally we should have a do, try, catch which can catch the error if the JSONSerialization throws an error
                        //But as I have full controll over the web application I omitted this step (Meaning, if the json **should** never be invalid)
                        let json = try? JSONSerialization.jsonObject(with: unwrappedData)
                        var people: [Person] = []
                        if let scores = json as? [Dictionary<String, AnyObject>]{
                            for playerScore in scores {
                                
                                let name = playerScore["name"] as! String
                                let location = playerScore["location"] as! String
                                let score = playerScore["score"] as! Int
                                let category = playerScore["category"] as! String
                                let person = Person(name: name,location: location, score: String(score), category: category)
                                
                                people.append(person)
                            }
                            returnScores(people)
                        }else{
                            print("JSON invalid")
                            returnScores(nil)
                        }
                    }else{
                        print("error on the web application when trying to get scores")
                        returnScores(nil)
                    }
                }else{
                    print("Couldn't unwrap data")
                    returnScores(nil)
                }
            }
        }
        getJob.resume()
    }
}
