//
//  QuestionsStorage.swift
//  Quiztastic
//
//  Created by Omar Zidan on 2019-11-19.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import Foundation
import CoreData
import UIKit
class QuestionsStorage {
    
    static let globalQuestionStorage = QuestionsStorage()
    //MARK: Properties
    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var context: NSManagedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
    var currentQuestions: [quizQuestion] = []
    //MARK: Store questions locally
    func addCategoryToCoreData(nameOfCategory: String ,quizQuestions : [quizQuestion] ){
        DispatchQueue.main.async {
            self.context = self.appDelegate.persistentContainer.viewContext
            let categoryDescription = NSEntityDescription.entity(forEntityName: "CategoryEntity", in: self.context)!
            let quizQuestionDescription = NSEntityDescription.entity(forEntityName: "QuizQuestionEntity", in: self.context)!
            
            let categoryEntry = NSManagedObject(entity: categoryDescription, insertInto: self.context) as! CategoryEntity
            //sets the category name into the categoryType attribute that is in the CategoryEntity
            categoryEntry.setValue(nameOfCategory, forKey: "categoryType")
            
            var listOfQuizQuestion : [QuizQuestionEntity] = []
            // loops through the quizQuestions in the parameter of the function and makes quizquestion entry then adds it to the list
            for quizQuestion in quizQuestions{
                let quizQuestionEntry = NSManagedObject(entity: quizQuestionDescription, insertInto: self.context) as! QuizQuestionEntity
                quizQuestionEntry.setValue(quizQuestion.question, forKey: "question")
                quizQuestionEntry.setValue(quizQuestion.correct_answer, forKey: "correctAnswer")
                quizQuestionEntry.setValue(quizQuestion.incorrect_answers.joined(separator: "separator"), forKey: "incorrectAnswers")
                listOfQuizQuestion.append(quizQuestionEntry)
            }
            
            // when all the quiz questions added to the list of quizQuestion, we set the category entity's property "quizQuestion" that has relation to QuizQuestionEntity with help of NSSET.
            categoryEntry.setValue(NSSet(array: listOfQuizQuestion), forKey: "quizQuestion")
            
            do {
                // we save the category and its questions in the context.
                try self.context.save()
            }catch let error as NSError{
                print("Couldnt save data. \(error), \(error.userInfo)")
            }
        }
    }
    //MARK: Fetch questions for a game
    func get10QuestionsForGivenCategory(nameOfCategory: String){
        self.context = self.appDelegate.persistentContainer.viewContext 
        // A description of search criteria used to retrieve data from a persistent store.
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CategoryEntity")
        fetchRequest.predicate = NSPredicate(format: "categoryType == %@", nameOfCategory)
        fetchRequest.returnsObjectsAsFaults = false
        
        do{
            let fetchedCategory = try context.fetch(fetchRequest)
            for data in fetchedCategory as! [NSManagedObject]{
                let fetchedQuizQuestionProperty =  data.value(forKey:"quizQuestion") as! NSSet
                let fetchedListOfQuestions: [QuizQuestionEntity] = fetchedQuizQuestionProperty.allObjects as! [QuizQuestionEntity]
                var tmpQuizQuestions: [quizQuestion] = []
                for indexOfQuizQuestionEntity in fetchedListOfQuestions{
                    let incorrectAnswersArr = indexOfQuizQuestionEntity.incorrectAnswers!.components(separatedBy: "separator")
                    tmpQuizQuestions.append(quizQuestion(question: indexOfQuizQuestionEntity.question!, correct_answer: indexOfQuizQuestionEntity.correctAnswer!, incorrect_answers: incorrectAnswersArr))
                }
                currentQuestions = []
                
                // picks 10 random quiz questions from the 50 quizquestions fetched from core data.
                var randomNumber: Int
                for i in 0..<10 {
                    randomNumber = Int.random(in: 0..<50 - i)
                    currentQuestions.append(getDecodedHtmlString(question: tmpQuizQuestions[randomNumber]))
                    tmpQuizQuestions.remove(at: randomNumber)
                }
            }
        }catch let error as NSError{
            print("Couldnt save data. \(error), \(error.userInfo)")
        }
    }
    
    //MARK: Convert html to string
    func getDecodedHtmlString(question: quizQuestion) -> quizQuestion{
        var fixedQuestion: quizQuestion = quizQuestion()
        for incorrectAnswer in question.incorrect_answers {
            fixedQuestion.incorrect_answers.append(decodeHtmlString(encodedString: incorrectAnswer))
        }
        fixedQuestion.question = decodeHtmlString(encodedString: question.question)
        fixedQuestion.correct_answer = decodeHtmlString(encodedString: question.correct_answer)
        return fixedQuestion
    }
    
    func decodeHtmlString(encodedString: String) -> String{
        let data = Data(encodedString.utf8)
        if let attributedString = try? NSAttributedString(data : data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil){
            return attributedString.string
        }
        return "Couldnt decode the string"
    }
    
    //MARK: Checks if there is data in database
    func databaseIsEmpty() -> Bool{
        self.context = self.appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CategoryEntity")
        fetchRequest.predicate = NSPredicate(format: "categoryType == %@", "Music")
        do {
            let result = try context.fetch(fetchRequest)
            if(result.count == 1){
                return false
            }
        }catch{
            print("Communication with Core Data resulted in an error")
        }
        return true
    }
    
    //MARK: For testing purposes
    func deleteAllDataFromQuizEntity(){
        context = self.appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CategoryEntity")
        
        do{
            let result = try context.fetch(fetchRequest)
            for data in result as! [NSManagedObject]{
                context.delete(data)
            }
            
            try context.save()
            
        }catch{
            print("Failed deleting data")
        }
    }
}
