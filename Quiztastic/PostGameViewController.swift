//
//  PostGameViewController.swift
//  Quiztastic
//
//  Created by Omar Zidan on 2019-11-13.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import UIKit
import CoreLocation

class PostGameViewController: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var textLengthLabel: UILabel!
    @IBOutlet weak var statusMessage: UILabel!
    @IBOutlet weak var postGameView: UIView!
    @IBOutlet weak var totalScore: UILabel!
    @IBOutlet weak var uploadToDBButton: UIButton!
    @IBOutlet weak var questionsRight: UILabel!
    @IBOutlet weak var nameInput: UITextField!

    //MARK: Properties
    var finalScore: Int!
    var scoreHasBeenUploaded = false
    var locationTracking = false
    let maxLengthOfName = 15
    let locationManager = CLLocationManager()
    var endValue: Double = 0
    var displayLink: CADisplayLink? = nil
    var gameState: GameState!

    //MARK: View setup
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        nameInput.delegate = self
        self.navigationItem.hidesBackButton = true
        uploadToDBButton.titleLabel?.adjustsFontSizeToFitWidth = true
        postGameView.layer.insertSublayer(Aesthetics().getGradiantBackground(view: self.postGameView), at: 0)
        NavigationBarProperties().setNavBarTranslucent(viewController: self)
        questionsRight.text = ""
        totalScore.text = "0"
        endValue = Double(self.gameState.totalScore)
        //Providing a target object and a selector to be called when the screen is updated.
        displayLink = CADisplayLink(target: self, selector: #selector(handleUpdate))
        //To synchronize your display loop(the selector) with the display, your application adds it to a run loop(add fucntion)
        displayLink!.add(to: .main, forMode: .default)
        
    }

    //MARK: Button press event
    @IBAction func addingScoreToDatabase(_ sender: Any) {
        if(self.scoreHasBeenUploaded){
            setStatusText(message: "Score has already been uploaded", color: UIColor.green)
        }else if(self.nameInput.text == ""){
            setStatusText(message: "You need to enter a name", color: UIColor.red)
        }
        else if(self.nameInput.text!.count > self.maxLengthOfName){
            setStatusText(message: "Name is too long", color: UIColor.red)
        }else{
            if self.locationTracking {
                determineCurrentLocation()
            }else{
                if(self.scoreHasBeenUploaded == false){
                    self.uploadToDatabase(location: nil)
                }
                self.scoreHasBeenUploaded = true
            }
        }
    }

    //MARK: Database communications
    func uploadToDatabase(location: CLPlacemark?){
        
        Database().uploadScore(name: self.nameInput.text!, location: location?.locality ?? "" , score: String(gameState.totalScore), category: gameState.category) { (uploadStatus) in
            switch(uploadStatus){
            case .didUpload:
                let userMessage = "Score has been uploaded"
                DispatchQueue.main.async{self.setStatusText(message: userMessage, color: UIColor.green)}
                DispatchQueue.main.async{self.disableAndHideButton(button: self.uploadToDBButton)}
                break;
            case .uploadedLocallyOnly:
                let userMessage = "Score has been uploaded locally"
                DispatchQueue.main.async{self.setStatusText(message: userMessage, color: UIColor.green)}
                DispatchQueue.main.async{self.disableAndHideButton(button: self.uploadToDBButton)}
                break;
            case .failedUpload:
                //Future update will implement this
                break;
            }
        }
    }

    //MARK: View configurations
    func disableAndHideButton(button: UIButton){
        button.isEnabled = false
        button.alpha = 0
    }
    
    func setStatusText(message: String!, color: UIColor){
        self.statusMessage.textColor = color
        self.statusMessage.text = message
    }
    
    //Used to dissmiss the keyboard if the user press somewhere not on the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    var startValue: Double = 0
    let animationDuration: Double = 3
    let animationStartDate = Date()
    
    //The selector on the target (the function below) is called when the screen’s contents need to be updated.
    //Using display link and its associtaion with the run loop.
    @objc func handleUpdate(){
        let now = Date()
        let elapsedTime = now.timeIntervalSince(animationStartDate)
        
        if(elapsedTime > animationDuration){
            self.totalScore.text="\(Int(endValue))"
            slideInAnimation()
            self.displayLink!.remove(from: .main, forMode: .default)
        }else{
            let precentage = elapsedTime / animationDuration
            let value = startValue + precentage * (endValue - startValue)
            self.totalScore.text = "\(Int(value))"
        }
    }
    
    
    //MARK: - Animation
    
    func confettiAnimation(){
        let confettiView = ConfettiView(frame: self.view.bounds)
        confettiView.alpha = 0
        self.postGameView.insertSubview(confettiView, at: 1)
        confettiView.startConfetti()
        UIView.animate(withDuration: 2, animations: {
            confettiView.alpha = 1
        })
    }
    
    func slideInAnimation(){
        
        questionsRight.text = "You got \(String(gameState.rightAnswers))/\(String(gameState.totalQuestions)) questions right!"
        UIView.animate(withDuration: 2, delay: 0 ,usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.questionsRight.frame.origin.x = CGFloat(400)
        }, completion: {_ in
            if(self.gameState.rightAnswers > 0){
                self.questionsRight.center = self.postGameView.center
                self.confettiAnimation()
            }
        })
    }
}

//MARK: - CLLocationManagerDelegate
extension PostGameViewController: CLLocationManagerDelegate {
    
    func lookUpCurrentLocation(completionHandler: @escaping (CLPlacemark?) -> Void){
        if let lastLocation = self.locationManager.location{
            let geocoder = CLGeocoder()
            geocoder.reverseGeocodeLocation(lastLocation, completionHandler:  { (placemarks, error) in
                if error == nil {
                    //Apple notes uses placemarks?[0]. I have not found a difference between placemarks?[0] and placemarks.
                    //But as apple used it in thier example I do it as well.
                    let firstLocation = placemarks?[0]
                    completionHandler(firstLocation)
                }else{
                    //An error occurred during goecoding
                    completionHandler(nil)
                }
            })
        }else{
            // No location was available
            completionHandler(nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        lookUpCurrentLocation { (placemark) in
            if(self.locationTracking){
                if(self.scoreHasBeenUploaded == false){
                    self.uploadToDatabase(location: placemark)
                }
                self.scoreHasBeenUploaded = true
            }else{
                if(self.scoreHasBeenUploaded == false){
                    self.uploadToDatabase(location: nil)
                }
                self.scoreHasBeenUploaded = true
            }
        }
        manager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch(status){
        case .authorizedAlways, .authorizedWhenInUse:
            self.locationTracking = true
            break
        case .restricted, .denied:
            self.locationTracking = false
            break
        case .notDetermined:
            determineCurrentLocation()
            break
        @unknown default:
            break
        }
    }
    
    func determineCurrentLocation(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        if self.locationTracking{
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error\(error)")
    }
}

//MARK: - UITextFieldDelegate
extension PostGameViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentText = textField.text ?? ""
        
        if (currentText.count > 10){
             textLengthLabel.text = "\(String(currentText.count))/\(self.maxLengthOfName)"
        }else{
            textLengthLabel.text = ""
        }
        
        //if backspace is pressed string = "" (Note that this could catch other operation than backspace, example a cut operation)
        if (string == ""){
            return true
        }
        
        //Function returns true if text field should update, false if it shouldn't
        return currentText.count < self.maxLengthOfName
    }
    
    //Make it possible to upload score by pressing return on iOS keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        addingScoreToDatabase(self)
        return true
    }
}
