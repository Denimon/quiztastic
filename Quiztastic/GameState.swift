//
//  GameState.swift
//  Quiztastic
//
//  Created by Conrad Tingström on 2019-11-18.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import Foundation

struct GameState {
    let category: String!
    let totalQuestions = 10
    var quizQuestions: [quizQuestion]
    var questionsAnswered: Int!
    var rightAnswers: Int!
    var totalScore: Int!
    
    init(category: String = "", currentQuizQuestions: [quizQuestion] = [], questionsAnswered: Int = 0, rightAnswers: Int = 0, totalScore: Int = 0) {
        self.category = category
        self.quizQuestions = currentQuizQuestions
        self.questionsAnswered = questionsAnswered
        self.rightAnswers = rightAnswers
        self.totalScore = totalScore
    }
}
