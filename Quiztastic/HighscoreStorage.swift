//
//  HighscoreStorage.swift
//  Quiztastic
//
//  Created by Omar Zidan on 2019-11-19.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import Foundation
import UIKit
import CoreData

public class HighscoreStorage: NSObject {
    
    static let globalHighscoreStorage = HighscoreStorage()
    //MARK: Properties
    let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var context: NSManagedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
    
    //MARK: Store a player's score into core data
    func addPlayerToHighscore(newPlayerEntry: Person)  {
        context = self.appDelegate.persistentContainer.viewContext
        let highscoreEntityDescription = NSEntityDescription.entity(forEntityName: "Highscore", in: context)!
        
        let highscoreEntity = NSManagedObject(entity: highscoreEntityDescription, insertInto: context) as! Highscore
        highscoreEntity.setValue(newPlayerEntry.category, forKey: "categoryName")
        highscoreEntity.setValue(newPlayerEntry.name, forKey: "name")
        highscoreEntity.setValue(Int16(newPlayerEntry.score), forKey: "score")
        highscoreEntity.setValue(newPlayerEntry.location, forKey: "location")
        
        do {
            try context.save()
        }catch let error as NSError{
            print("Couldnt save data. \(error), \(error.userInfo)")
        }
    }
    
    //MARK: Fetch highscores from core data.
    func getHighScoreForGivenType(typeOfCategoryGroup: String)-> [Person]{
        self.context = self.appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Highscore")
        let sortQuery = NSSortDescriptor(key: "score", ascending: false)
        fetchRequest.sortDescriptors = [sortQuery]
        if(typeOfCategoryGroup != "All"){
            fetchRequest.predicate = NSPredicate(format: "categoryName == %@", typeOfCategoryGroup)
        }
        var playersScores : [Person] = []
        do {
            let fetchedScores =  try context.fetch(fetchRequest)
            for indexOfPlayerScore in fetchedScores as! [NSManagedObject]{
                let categoryName = indexOfPlayerScore.value(forKey: "categoryName")
                let playerName = indexOfPlayerScore.value(forKey: "name")
                var playerScore = indexOfPlayerScore.value(forKey: "score")
                playerScore = "\(playerScore!)"
                let playerLocation = indexOfPlayerScore.value(forKey: "location")
                playersScores.append(Person(name: playerName as! String, location: playerLocation as! String, score: playerScore as! String, category: categoryName as! String))
            }
        } catch {
            print("Couldnt fetch highscore for all categories!")
        }
        return playersScores
    }
    func getTwoHighestScore() -> [Person]{
        self.context = self.appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Highscore")
        let sortQuery = NSSortDescriptor(key: "score", ascending: false)
        fetchRequest.sortDescriptors = [sortQuery]
        fetchRequest.fetchLimit = 2
        
        var topTwoPlayerList : [Person] = []
        do {
            let fetchedTwoHighestScore = try context.fetch(fetchRequest)
            for indexOfPlayerScore in fetchedTwoHighestScore as! [NSManagedObject]{
                let categoryName = indexOfPlayerScore.value(forKey: "categoryName")
                let playerName = indexOfPlayerScore.value(forKey: "name")
                var playerScore = indexOfPlayerScore.value(forKey: "score")
                // this is happening just to typecast playerscore that is int to string
                playerScore = "\(playerScore!)"
                let playerLocation = indexOfPlayerScore.value(forKey: "location")
                topTwoPlayerList.append(Person(name: playerName as! String, location: playerLocation as! String, score: playerScore as! String, category: categoryName as! String))
            }
        } catch {
            print("Couldnt fetch top highscores")
        }
        return topTwoPlayerList
    }
    
    //MARK: For testing purposes
    func deleteAllDataFromHighscoreEntity(){
        context = self.appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Highscore")
        fetchRequest.returnsObjectsAsFaults = false
        
        do{
            let result = try context.fetch(fetchRequest)
            for data in result as! [NSManagedObject]{
                context.delete(data)
            }
            
            try context.save()
            
        }catch{
            print("failed deleting data!!")
        }
    }
    
}
