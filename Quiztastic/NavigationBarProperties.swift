//
//  NavigationBarProperties.swift
//  Quiztastic
//
//  Created by Omar Zidan on 2019-11-13.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import UIKit

class NavigationBarProperties {
    func setNavBarTranslucent(viewController: UIViewController) {
        viewController.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        viewController.navigationController?.navigationBar.shadowImage = UIImage()
        viewController.navigationController?.navigationBar.isTranslucent = true
    } 
}
