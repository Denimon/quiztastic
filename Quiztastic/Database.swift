//
//  Database.swift
//  Quiztastic
//
//  Created by Adam Leo on 2019-11-19.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import Foundation

class Database{
    
    enum uploadState{
        case failedUpload, didUpload, uploadedLocallyOnly
    }
    
    func getScore(category: String?, isLocalMode: Bool, getScores: @escaping (_ people: [Person]?) -> Void){
        if isLocalMode{
            getScores(HighscoreStorage.globalHighscoreStorage.getHighScoreForGivenType(typeOfCategoryGroup: category ?? "All"))
        } else {
            OnlineDatabase().getScores(category: category, returnScores: { people in
                getScores(people)
            })
        }
    } 
    
    func uploadScore(name: String, location: String, score: String, category: String, uploaded: @escaping (_ uploadStatus: uploadState) -> Void){
        OnlineDatabase().uploadScore(name: name, location: location, score: score, category: category, couldUpload: { success in
            DispatchQueue.main.async{HighscoreStorage.globalHighscoreStorage.addPlayerToHighscore(newPlayerEntry: Person(name: name, location: location, score: score, category: category))}
            if success{
                 uploaded(uploadState.didUpload)
            }else{
                uploaded(uploadState.uploadedLocallyOnly)
            }
        })
    }
}
