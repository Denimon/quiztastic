//
//  HighscoreTableViewCell.swift
//  Quiztastic
//
//  Created by Adam Leo on 2019-11-12.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import UIKit

class HighscoreTableViewCell: UITableViewCell {
    @IBOutlet weak var placement: UILabel!
    @IBOutlet weak var categoryIcon: UIImageView!
    @IBOutlet weak var place: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var score: UILabel!
}
