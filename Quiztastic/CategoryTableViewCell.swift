//
//  CategoryTableViewCell.swift
//  Quiztastic
//
//  Created by Johan Kantola on 2019-11-13.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
}
