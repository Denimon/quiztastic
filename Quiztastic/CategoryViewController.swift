//
//  CategoryViewController.swift
//  Quiztastic
//
//  Created by Omar Zidan on 2019-11-13.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var categoryTableView: UITableView!
    @IBOutlet var categoryView: UIView!
    
    //MARK: Properties
    var activityView: UIView?

    //MARK: View setup
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryView.layer.insertSublayer(Aesthetics().getGradiantBackground(view: self.categoryView), at: 0)
        NavigationBarProperties().setNavBarTranslucent(viewController: self)
        
        categoryTableView.delegate = self
        categoryTableView.dataSource = self
        
        categoryTableView.reloadData()
        
    } 
}
//MARK: UITableViewDelegate

extension CategoryViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Category.categories.count - 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "categoryViewCell", for: indexPath) as? CategoryTableViewCell {
            cell.categoryLabel.text = Category.categories[indexPath.row + 1]
            cell.categoryImage.image = UIImage(named: Category.categoryImg[Category.categories[indexPath.row + 1]] ?? "Logo")
            
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: cell.frame.width, height: cell.frame.height))
            let image = UIImage(named: "normalCell")
            imageView.image = image
            cell.backgroundView = imageView
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.showSpinner()
        QuestionsStorage.globalQuestionStorage.get10QuestionsForGivenCategory(nameOfCategory: Category.categories[indexPath.row + 1])
        self.removeSpinner()
        performSegue(withIdentifier: "segueToGame", sender: Category.categories[indexPath.row + 1])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let gameViewController = segue.destination as? GameViewController {
            gameViewController.gameState = GameState(category: sender as! String, currentQuizQuestions: QuestionsStorage.globalQuestionStorage.currentQuestions)
        }
    }
    
    //MARK: Spinner
    
    func showSpinner (){
        self.navigationItem.hidesBackButton = true
        activityView = UIView(frame : self.view.bounds)
        activityView?.backgroundColor = UIColor.init(red: 0.1, green: 0.1, blue: 0.15, alpha: 0.8)
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.color = .white
        activityIndicator.center = activityView!.center
        activityIndicator.startAnimating()
        activityView?.addSubview(activityIndicator)
        self.view.addSubview(activityView!)
    }
    func removeSpinner(){
        self.navigationItem.hidesBackButton = false
        activityView?.removeFromSuperview()
        activityView = nil
        
    }


}


