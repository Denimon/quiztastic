//
//  HighscoreViewController.swift
//  Quiztastic
//
//  Created by Adam Leo on 2019-11-12.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import UIKit

class HighscoreViewController: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var categoryText: UILabel!
    @IBOutlet weak var rightCategoryArrow: UIBarButtonItem!
    @IBOutlet weak var leftCategoryArrow: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var highscoreView: UIView!

    //MARK: Properties
    let bestCellBackground: [String] = ["goldCell2","silverCell2","bronzeCell2"]
    var categoryIndex = 0
    var people: [Person]?
    var localMode = false

    //MARK: View setup
    override func viewDidLoad() {
        super.viewDidLoad()
        highscoreView.layer.insertSublayer(Aesthetics().getGradiantBackground(view: highscoreView), at: 0)
        categoryText.text = Category.categories[0]
        tableView.delegate = self
        tableView.dataSource = self
        fetchScoresFromDB()
    }
    
    //MARK: Database communications
    func fetchScoresFromDB(){
        Database().getScore(category: Category.categories[categoryIndex], isLocalMode: localMode, getScores: {
            scores in
            self.people = scores
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
    }
    
    //MARK: Button press events
    @IBAction func leftCategory(_ sender: Any) {
        categoryIndex = ((categoryIndex-1)+Category.categories.count)%Category.categories.count //To handle negtaive number in modules
        categoryText.text = Category.categories[categoryIndex]
        fetchScoresFromDB()
    }
    
    @IBAction func rightCategory(_ sender: Any) {
        categoryIndex = (categoryIndex + 1)%Category.categories.count
        categoryText.text = Category.categories[categoryIndex]
        fetchScoresFromDB()
    }
    

    
    @IBAction func changeLocalMode(_ sender: Any) {
        localMode = localMode == false ? true : false
        fetchScoresFromDB()
    }
}

// MARK: UITableViewDelegate
    
extension HighscoreViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.people?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "normal", for: indexPath) as? HighscoreTableViewCell {
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: cell.frame.width, height: cell.frame.height))
            if indexPath.row < 3{
                imageView.image = UIImage(named: bestCellBackground[indexPath.row%3])
                cell.placement.text = ""
            } else {
                imageView.image = UIImage(named: "normalCell")
                cell.placement.text = String(indexPath.row+1)
            }
            cell.backgroundView = imageView
            cell.score.text = people?[indexPath.row].score ?? ""
            cell.name.text = people?[indexPath.row].name ?? ""
            cell.place.text = people?[indexPath.row].location ?? ""
            cell.categoryIcon.image = UIImage(named: Category.categoryImg[people?[indexPath.row].category ?? "None"] ?? "Logo")
            
            return cell
            
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 66
    }
}
