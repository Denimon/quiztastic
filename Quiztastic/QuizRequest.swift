//
//  QuizRequest.swift
//  Quiztastic
//
//  Created by Johan Kantola on 2019-11-19.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import Foundation




class QuizRequest {
    
    static let globaLQuizRequest =  QuizRequest()
    
    var resourceURL: URL! = URL(string: "")
    
    enum QuizError: Error{
        case noDataAvailable
        case canNotProcessData
    }

    
    func setup(amountOfQuestions: Int, category: Int){
        
        let resourceString = "https://opentdb.com/api.php?amount=\(amountOfQuestions)&category=\(category)&type=multiple"
        
        guard let resourceURL = URL(string: resourceString) else {fatalError()}
        self.resourceURL = resourceURL
    }
    func getQuiz (completion: @escaping(Result<[quizQuestion], QuizError>) ->Void) {
            let dataTask = URLSession.shared.dataTask(with: resourceURL) { data, _, error in
                
                guard let jsonData = data else {
                    completion(.failure(.noDataAvailable))
                    return
                }

                do{
                    let retrievedQuizAttributes = try JSONDecoder().decode(listOfQuizQuestions.self, from: jsonData)
                    completion(.success(retrievedQuizAttributes.results))
                    
                }catch{
                    print(error)
                    completion(.failure(.canNotProcessData))
                    
                }

            }
            dataTask.resume()
        }
    }
