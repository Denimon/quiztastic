//
//  QuiztasticTests.swift
//  QuiztasticTests
//
//  Created by Omar Zidan on 10/11/19.
//  Copyright © 2019 QuizstasticAB. All rights reserved.
//

import XCTest
@testable import Quiztastic

class QuiztasticTests: XCTestCase {
    
    func testFetchQuizFromApi() {
        
        let expectation = XCTestExpectation(description: "Generate Quiz")
        QuizRequest.globaLQuizRequest.setup(amountOfQuestions: 10,category: 23)
        QuizRequest.globaLQuizRequest.getQuiz { result in
            
            switch result {
                
            case .failure(let error):
                XCTAssertNotNil(error)
                XCTAssert(QuizRequest.QuizError.noDataAvailable == error || QuizRequest.QuizError.canNotProcessData == error )
                expectation.fulfill()
                
            case .success(let questions):
                
                if questions.count != 0{
                    XCTAssert(questions.count == 10)
                    XCTAssert(questions[0].question != "")
                    expectation.fulfill()
                }
                else{
                    XCTAssert(questions.count == 0)
                    expectation.fulfill()
                }
            }
        }
        wait(for:[expectation], timeout: 10.0)
        
    }
    
    func testGetScoresFromDB() {  //test to fetch highscores from a certain category
        
        let expectation = XCTestExpectation(description: "Fetch data from db")
        var people: [Person]?
        
        Database().getScore(category: "Music", isLocalMode: false, getScores: {  scores in
            
            people = scores
            XCTAssert(people?.count != 0)
            XCTAssertEqual(people?[0].category, "Music")
            expectation.fulfill()
            
        })
        wait(for:[expectation], timeout: 10.0)
    }
    
    
    func testQuestionStorage() {
        
        XCTAssertFalse(QuestionsStorage.globalQuestionStorage.databaseIsEmpty())
        for index in 1..<Category.categories.count {
            QuestionsStorage.globalQuestionStorage.get10QuestionsForGivenCategory(nameOfCategory: Category.categories[index])
            XCTAssert(QuestionsStorage.globalQuestionStorage.currentQuestions.count == 10)
            for question in QuestionsStorage.globalQuestionStorage.currentQuestions{
                XCTAssertFalse(question.question == "")
                XCTAssertFalse(question.incorrect_answers[0] == "")
                XCTAssertFalse(question.incorrect_answers[1] == "")
                XCTAssertFalse(question.incorrect_answers[2] == "")
                XCTAssertFalse(question.correct_answer == "")
            }
            
        }
    }
    
    func testgameState(){
        
        QuestionsStorage.globalQuestionStorage.get10QuestionsForGivenCategory(nameOfCategory: "Music")
        XCTAssertEqual(QuestionsStorage.globalQuestionStorage.currentQuestions.count, 10)
        var gameState: GameState!
        gameState = GameState(category: "Music", currentQuizQuestions: QuestionsStorage.globalQuestionStorage.currentQuestions)
        XCTAssertEqual(gameState.category, "Music")
        
        var pickedQuestionFromGameState: quizQuestion = quizQuestion()

        XCTAssert(gameState.totalQuestions == 10)
        gameState.questionsAnswered = 5
        
        let randomQuestionIndex = Int.random(in: 0..<gameState.totalQuestions - gameState.questionsAnswered)
        XCTAssert(randomQuestionIndex < 5 && randomQuestionIndex >= 0)
        
        pickedQuestionFromGameState = gameState.quizQuestions[randomQuestionIndex]
        XCTAssertFalse(pickedQuestionFromGameState.correct_answer == "")
        XCTAssertFalse(pickedQuestionFromGameState.question == "")
        XCTAssertFalse(pickedQuestionFromGameState.correct_answer == "")
        XCTAssertFalse(pickedQuestionFromGameState.incorrect_answers[0] == "")
        XCTAssertFalse(pickedQuestionFromGameState.incorrect_answers[1] == "")
        XCTAssertFalse(pickedQuestionFromGameState.incorrect_answers[2] == "")
        XCTAssert(pickedQuestionFromGameState.incorrect_answers.count == 3)
        
        gameState.quizQuestions.remove(at: randomQuestionIndex)
        XCTAssertFalse(gameState.quizQuestions.count == 10)
        
        XCTAssert(pickedQuestionFromGameState.incorrect_answers.popLast() != "")
        XCTAssert(pickedQuestionFromGameState.incorrect_answers.count == 2)
        
        
    }
    
    func testDeleteQuizQuestions(){
        QuestionsStorage.globalQuestionStorage.deleteAllDataFromQuizEntity()
        XCTAssert(QuestionsStorage.globalQuestionStorage.databaseIsEmpty())
    }
}

